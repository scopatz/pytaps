from distutils.core import setup, Extension
import os

def find_itaps_dir(iface):
    # Adapted from setup.py in the root directory: try a bunch of places where
    # iMesh.h might be located.
    search = ( [os.path.join(i, '..', 'include') for i in
                os.getenv('PATH',  "").split(':') +
                os.getenv('CPATH', "").split(':') +
                os.getenv('LD_LIBRARY_PATH', "").split(':')] + 
               ['/usr/local/include', '/usr/include'] )

    for i in search:
        name = os.path.join(i, iface+'.h')
        if os.path.isfile(name):
            return i

setup(name = 'C Extension Example',
      version = '1.0',
      description = 'A simple example showing how to extend PyTAPS from C',
      author = 'Jim Porter',
      author_email = 'jvporter@wisc.edu',
      ext_modules = [
        Extension('itaps_ext_example',
                  sources = ['example.c'],
                  include_dirs = [find_itaps_dir('iMesh')],
                  )
        ],
      )
